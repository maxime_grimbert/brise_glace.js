# Brise-glace en quatre rounds

Outil pour lister les invité·e·s et indiquer si iels appartiennent à des groupes en commun et leur participation effective puis pour consulter un tableau (généré automatiquement) de quatre rencontres maximum pour chaque participant·e·s présent·e le premier matin, excluant les répétitions et si possible les rencontres entre personnes du même groupe.

## Pour utiliser (français)

-[ ] Télécharger l'ensemble de ce dépôt (ou seulement les trois fichiers respectivement .html, .css et .js)
-[ ] Placer ces trois fichiers où l'on veut, dans le même dossier
-[ ] Ouvrir le fichier .html dans le navigateur (idéalement, avec Mozilla Firefox)

## How to use (english)

-[ ] Download the whole repository (or only the three files that are respectively .html, .css and .js)
-[ ] Move those three files wherever, in the same folder
-[ ] Open the file .html in a web browser (ideally with Mozilla Firefox)
